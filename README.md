# Spécialité Numérique et Sciences Informatiques - Classe de Première

*Stéphane Ramstein : <stephane.ramstein@ac-lille.fr>*

*Enseignant d'Informatique et de Physique-Chimie au lycée Raymond Queneau de Villeneuve d'Ascq*

--------------------

Ensemble de ressources et activités autour du programme de la spécialité NSI.

La page a changé d'adresse. Cliquer sur les liens ci-dessous :

* [Dépôt GitLab](https://gitlab.com/stephane_ramstein/nsi)

* [Page web du dépôt](https://stephane_ramstein.gitlab.io/nsi)

--------------------
